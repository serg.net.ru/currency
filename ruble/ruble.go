package ruble

import (
	"crypto/tls"
	"net/http"
	"strconv"
	"strings"

	"github.com/sbabiv/xml2map"
)

const (
	CBR = "https://www.cbr-xml-daily.ru/daily_utf8.xml"
)

type RUB struct {
	USD float64 `json:"usd"`
	EUR float64 `json:"eur"`
}

func (o *RUB) Refresh() error {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	res, err := client.Get(CBR)
	if err != nil {
		return err
	}

	decoder := xml2map.NewDecoder(res.Body)
	currencies, err := decoder.Decode()
	if err != nil {
		return err
	}

	for _, v := range currencies["ValCurs"].(map[string]interface{})["Valute"].([]map[string]interface{}) {
		if v["CharCode"] == "USD" {
			val, err := o.parse(v["Value"].(string))
			if err != nil {
				return err
			}
			o.USD = val
		}
		if v["CharCode"] == "EUR" {
			val, err := o.parse(v["Value"].(string))
			if err != nil {
				return err
			}
			o.EUR = val
		}
	}
	return nil
}

func (o *RUB) parse(val string) (float64, error) {
	s := strings.Replace(val, ",", ".", -1)
	parsed, err := strconv.ParseFloat(
		s,
		64,
	)
	if err != nil {
		return 0, err
	}
	return parsed, nil
}
