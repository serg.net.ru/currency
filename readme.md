```go
package main

import (
	"log"

	"github.com/k0kubun/pp"
	
	"gitlab.com/serg.net.ru/currency/ruble"
)

func main() {
	r := &ruble.RUB{}
	err := r.Refresh()
	if err != nil {
		log.Println(err)
		return
	}
	pp.Println(r)
}
```

```bash
&ruble.RUB{
  USD: 90.842300,
  EUR: 98.544700,
}
```